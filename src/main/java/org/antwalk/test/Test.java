package org.antwalk.test;


import java.util.List;


import org.antwalk.Dao.ValentineMapping;
import org.antwalk.model.Valentine;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
	public static void main(String[] args) {
		ApplicationContext context=new ClassPathXmlApplicationContext("beans.xml");
		ValentineMapping v=(ValentineMapping)context.getBean("ValentineMapping");
		List<Valentine>phLi=v.listBook();
		for(Valentine p:phLi)
		{
			System.out.println("Date:"+p.getDate());
			System.out.println("Gifts:"+p.getGifts());
			System.out.println("Location:"+p.getLocation());
		}
	}
}
